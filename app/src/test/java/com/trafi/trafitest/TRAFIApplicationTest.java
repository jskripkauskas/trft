package com.trafi.trafitest;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class TRAFIApplicationTest {
    TRAFIApplication trafiApplication;

    @Before
    public void setUp() {
        trafiApplication = Mockito.mock(TRAFIApplication.class);
    }

    @Test
    public void testGetInstance() throws Exception {
        Assert.assertNotNull("Application should be initialised", TRAFIApplication.getInstance());
    }

    @Test
    public void testGetSqliteHelper() throws Exception {
        Assert.assertNotNull("Local database should be initialised", TRAFIApplication.getSqliteHelper());
    }

}