package com.trafi.trafitest.animations;

import com.trafi.trafitest.BuildConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class MarkerAnimationTest {

    @Test
    public void testAnimateMarkerToGB() throws Exception {

    }

    @Test
    public void testAnimateMarkerToAngle() throws Exception {

    }
}