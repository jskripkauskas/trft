package com.trafi.trafitest;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class LocalDatabaseHelperTest {
    TRAFIApplication trafiApplication;
    LocalDatabaseHelper localDatabaseHelper;

    @Before
    public void setUp() {
        trafiApplication = Mockito.mock(TRAFIApplication.class);
        localDatabaseHelper = TRAFIApplication.getSqliteHelper();
    }

    @Test
    public void testImageDB() throws Exception {
        String original = "Original";
        String local = "Local";
        localDatabaseHelper.addImage(original, local);
        String localUrl = localDatabaseHelper.getImageMapping(original);

        Assert.assertEquals("Retrieved mapping should equal to saved local path.", localUrl, local);
    }

}