package com.trafi.trafitest.fragments;

import com.trafi.trafitest.BuildConfig;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

import static org.junit.Assert.*;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class TRAFIMapFragmentTest {

    @Test
    public void testBusOnOffClick() throws Exception {

    }

    @Test
    public void testUserLocationClick() throws Exception {

    }

    @Test
    public void testSetupGMap() throws Exception {

    }

    @Test
    public void testSetUpMap() throws Exception {

    }

    @Test
    public void testSetCurrentLocation() throws Exception {

    }

    @Test
    public void testOnCameraChange() throws Exception {

    }

    @Test
    public void testPlaceMarkers() throws Exception {

    }

    @Test
    public void testProcessMapMarker() throws Exception {

    }

    @Test
    public void testGetMarkerIcon() throws Exception {

    }

    @Test
    public void testRemoveOldMarkers() throws Exception {

    }

    @Test
    public void testOnMapClick() throws Exception {

    }

    @Test
    public void testOnMarkerClick() throws Exception {

    }
}