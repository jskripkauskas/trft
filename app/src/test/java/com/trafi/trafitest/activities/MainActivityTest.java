package com.trafi.trafitest.activities;

import android.widget.FrameLayout;

import com.trafi.trafitest.BuildConfig;
import com.trafi.trafitest.R;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class MainActivityTest {
    MainActivity mainActivity;

    @Before
    public void setUp() {
        mainActivity = Robolectric.buildActivity(MainActivity.class).create().resume().get();
    }

    @Test
    public void testInitialise() throws Exception {
        FrameLayout frameLayout = (FrameLayout) mainActivity.findViewById(R.id.map_layout);
        int viewsCount = frameLayout.getChildCount();

        Assert.assertTrue("Map should be added to view group.", viewsCount >= 1);
    }
}