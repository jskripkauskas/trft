package com.trafi.trafitest;

import android.graphics.Bitmap;

import junit.framework.Assert;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, emulateSdk = 21)
public class UtilTest {

    @Test
    public void testGetRoundedCornerBitmap() throws Exception {
        Bitmap simpleBitmap = Bitmap.createBitmap(1024, 1024, Bitmap.Config.ARGB_8888);
        Bitmap roundedBitmap = Util.getRoundedCornerBitmap(simpleBitmap, 20);
        Assert.assertNotNull("Rounded bitmap should not be null.", roundedBitmap);
    }
}