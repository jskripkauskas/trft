package com.trafi.trafitest.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trafi.trafitest.R;
import com.trafi.trafitest.TRAFIApplication;
import com.trafi.trafitest.Util;
import com.trafi.trafitest.animations.MarkerAnimation;
import com.trafi.trafitest.interfaces.AngleInterpolator;
import com.trafi.trafitest.interfaces.LatLngInterpolator;
import com.trafi.trafitest.interfaces.MapInterface;
import com.trafi.trafitest.models.LatitudeLongitude;
import com.trafi.trafitest.models.MapMarker;
import com.trafi.trafitest.models.MapMarkersList;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class TRAFIMapFragment extends Fragment implements GoogleMap.OnCameraChangeListener, GoogleMap.OnMapClickListener,
        GoogleMap.OnMarkerClickListener {
    //---------------------VARIABLES----------------------
    public static GoogleMap map;
    private View view;
    private MapInterface mapInterface;
    private Handler handler = new Handler();
    private boolean showBuses = true;
    protected HashMap<Marker, MapMarker> currentMapMarkers = new HashMap<>();
    private HashMap<String, Marker> currentMarkers;

    //---------------------LISTENERS----------------------

    Runnable refresh = new Runnable() {
        @Override
        public void run() {
            LatLngBounds bounds = map.getProjection().getVisibleRegion().latLngBounds;
//            Geocoder gcd = new Geocoder(getActivity(), Locale.getDefault());
//            LatLng placeCoordinates = bounds.getCenter();

            mapInterface.fetchData("vilnius", bounds);

//            try {
//                List<Address> addresses = gcd.getFromLocation(placeCoordinates.latitude, placeCoordinates.longitude, 1);
//                if (addresses.size() > 0) {
//                    mapInterface.fetchData(addresses.get(0).getLocality(), bounds);
//                }
//            } catch (IOException e) {
//                e.printStackTrace();
//            }

            handler.postDelayed(this, 5000l);
        }
    };

    @OnClick(R.id.bus_on_off_btn)
    public void busOnOffClick(View v) {
        ImageButton button = (ImageButton) v;

        if (v.getTag() == false) {
            button.setImageResource(R.drawable.ic_vehicles_on);
            v.setTag(true);
            showBuses = true;
            handler.post(refresh);
        } else {
            button.setImageResource(R.drawable.ic_vehicles_off);
            v.setTag(false);
            map.clear();
            currentMarkers.clear();
            showBuses = false;
            handler.removeCallbacks(refresh);
        }
    }

    @OnClick(R.id.user_location_btn)
    public void userLocationClick(View v) {
        ImageButton button = (ImageButton) v;

        if (v.getTag() == true) {
            button.setImageResource(R.drawable.ic_current_location_off);
            v.setTag(false);
            map.setMyLocationEnabled(false);
        } else {
            button.setImageResource(R.drawable.ic_current_location_on);
            v.setTag(true);
            map.setMyLocationEnabled(true);
        }
    }

    //----------------------STATUS------------------------

    @Inject
    public TRAFIMapFragment(){}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mapInterface = (MapInterface) getActivity();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }
        try {
            view = inflater.inflate(R.layout.fragment_map, container, false);
        } catch (InflateException e) {
            /* map is already there, just return view as it is */
        }

        ButterKnife.inject(this, view);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        setupGMap();
    }

    //--------------------MAIN-METHODS--------------------

    /**
     * Method setup google maps and required listeners.
     */
    protected void setupGMap() {
        // Get a handle to the Map Fragment
        if (map == null) {
            // Try to obtain the map from the SupportMapFragment.
            map = ((SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map)).getMap();
        }

        if (map != null) {
            setUpMap();
        }
    }

    /**
     * Setup map with data input. Show routes.
     */
    protected void setUpMap() {
        map.setOnCameraChangeListener(this);
        map.setOnMarkerClickListener(this);
        map.setOnMapClickListener(this);
        UiSettings mapSettings = map.getUiSettings();
        mapSettings.setMyLocationButtonEnabled(false);
        mapSettings.setRotateGesturesEnabled(false);
        currentMarkers = new HashMap<>();

        handler.post(refresh);
    }

    public void setCurrentLocation(Location location) {
        if (map != null) {
            map.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12));
        }
    }

    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
        if (map != null) {
            handler.removeCallbacks(refresh);
            handler.postDelayed(refresh, 3000l);
        }
    }

    /**
     * Place markers on map.
     */
    public void placeMarkers(MapMarkersList markersList) {
        if (!showBuses) {
            return;
        }

        List<MapMarker> mapMarkers = markersList.getMapMarkers();
        HashMap<String, Marker> newMarkers = new HashMap<>();
        currentMapMarkers.clear();

        for (MapMarker mapMarker : mapMarkers) {
            if (mapMarker.getType() == 2) {
                Marker marker = processMapMarker(mapMarker);
                currentMapMarkers.put(marker, mapMarker);
                newMarkers.put(mapMarker.getVehicleId(), marker);
            }
        }


        removeOldMarkers();
        currentMarkers = newMarkers;
    }

    protected Marker processMapMarker(MapMarker mapMarker) {
        Marker marker;
        MarkerOptions markerOptions = new MarkerOptions();
        LatitudeLongitude latitudeLongitude = mapMarker.getCoordinates();
        LatLng latLng = new LatLng(latitudeLongitude.getLat(), latitudeLongitude.getLng());
        Bitmap bm = getMarkerIcon(mapMarker);

        if (currentMarkers.containsKey(mapMarker.getVehicleId())) {
            marker = currentMarkers.get(mapMarker.getVehicleId());
            marker.setIcon(BitmapDescriptorFactory.fromBitmap(bm));
            float angle = mapMarker.getAngle();
            if (angle < 90 && marker.getRotation() > 270) {
                marker.setRotation(marker.getRotation() - 360);
            } else if (angle > 270 && marker.getRotation() < 90) {
                marker.setRotation(marker.getRotation() + 360);
            }

            MarkerAnimation.animateMarkerToAngle(marker, angle, new AngleInterpolator.SimpleInterpolator());
            MarkerAnimation.animateMarkerToGB(marker, latLng, new LatLngInterpolator.Linear());
            currentMarkers.remove(mapMarker.getVehicleId());
        } else {
            markerOptions.position(latLng);
            markerOptions.rotation(mapMarker.getAngle());
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(bm));
            marker = map.addMarker(markerOptions);
        }

        return marker;
    }

    protected Bitmap getMarkerIcon(MapMarker mapMarker) {
        String uri = Uri.parse(getString(R.string.icon_request))
                .buildUpon()
                .appendQueryParameter("style", "androidv2")
                .appendQueryParameter("size", mapMarker.getMapIconSize() + "")
                .appendQueryParameter("src", mapMarker.getMapIcon())
                .appendQueryParameter("cl", "ffffff")
                .build().toString();
        String localUri = TRAFIApplication.getSqliteHelper().getImageMapping(uri);

        Bitmap bm = BitmapFactory.decodeResource(getResources(), R.drawable.ic_vehicle_bg);
        bm = bm.copy(bm.getConfig(), true);
        int[] allPixels = new int[bm.getHeight() * bm.getWidth()];
        Util.replaceColor(allPixels, Color.parseColor("#000000"), Color.parseColor("#" + mapMarker.getColor()), bm);

        if (localUri.equals("")) {
            mapInterface.downloadIcon(uri);
        } else {
            Canvas canvas = new Canvas(bm);
            Bitmap bitmap = BitmapFactory.decodeFile(new File(localUri).getAbsolutePath());
            bitmap = Bitmap.createScaledBitmap(bitmap, bm.getWidth() / 2, bm.getHeight() / 2, true);
            canvas.drawBitmap(bitmap, bitmap.getWidth() / 2, bitmap.getHeight() / 2, null);
        }
        return bm;
    }

    protected void removeOldMarkers() {
        Set<String> currentMarkersKeys = currentMarkers.keySet();
        for (String markerKey : currentMarkersKeys) {
            Marker marker = currentMarkers.get(markerKey);
            marker.remove();
        }
    }

    @Override
    public void onMapClick(LatLng latLng) {
        mapInterface.hideTooltip();
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        MapMarker mapMarker = currentMapMarkers.get(marker);
        mapInterface.showTooltip(mapMarker);
        return true;
    }
}
