package com.trafi.trafitest.activities;

import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.gson.Gson;
import com.trafi.trafitest.R;
import com.trafi.trafitest.TRAFIApplication;
import com.trafi.trafitest.Util;
import com.trafi.trafitest.animations.CollapseExpandAnimator;
import com.trafi.trafitest.fragments.TRAFIMapFragment;
import com.trafi.trafitest.interfaces.MapInterface;
import com.trafi.trafitest.models.MapMarker;
import com.trafi.trafitest.models.MapMarkersList;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class MainActivity extends FragmentActivity implements MapInterface, GoogleApiClient.ConnectionCallbacks {
    //---------------------VARIABLES----------------------
    private Gson gson = new Gson();
    @Inject
    protected TRAFIMapFragment trafiMapFragment;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    @InjectView(R.id.tooltip_icon)
    protected ImageView tooltipIcon;
    @InjectView(R.id.tooltip_txt)
    protected TextView tooltipText;
    @InjectView(R.id.tooltip_layout)
    protected RelativeLayout tooltipView;
    private CollapseExpandAnimator tooltipAnimator;
    protected ThreadPoolExecutor threadPoolExecutor;

    //---------------------LISTENERS----------------------

    Response.Listener<String> trafiResponseListener = new Response.Listener<String>() {

        @Override
        public void onResponse(String response) {
            MapMarkersList markersList = gson.fromJson(response, MapMarkersList.class);
            trafiMapFragment.placeMarkers(markersList);
        }
    };

    Response.ErrorListener trafiErrorListener = new Response.ErrorListener() {
        @Override
        public void onErrorResponse(VolleyError error) {
        }
    };

    //----------------------STATUS------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.inject(this);
        TRAFIApplication.inject(this);
        initialise();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    //--------------------MAIN-METHODS--------------------

    protected void initialise() {
        buildGoogleApiClient();

        int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
        threadPoolExecutor = new ThreadPoolExecutor(NUMBER_OF_CORES, NUMBER_OF_CORES, 15, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());

        android.support.v4.app.FragmentManager fragmentManager = getSupportFragmentManager();
        android.support.v4.app.FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.map_layout, trafiMapFragment);
        fragmentTransaction.commit();

        tooltipAnimator = new CollapseExpandAnimator(tooltipView);
        tooltipAnimator.collapse();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        trafiMapFragment.setCurrentLocation(mLastLocation);
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void fetchData(String city, LatLngBounds bounds) {
        String uri = Uri.parse(getString(R.string.trafic_request))
                .buildUpon()
                .appendQueryParameter("userLocationId", city)
                .appendQueryParameter("southWestLat", bounds.southwest.latitude + "")
                .appendQueryParameter("southWestLng", bounds.southwest.longitude + "")
                .appendQueryParameter("northEastLat", bounds.northeast.latitude + "")
                .appendQueryParameter("northEastLng", bounds.northeast.longitude + "")
                .build().toString();

        StringRequest dataRequest = new StringRequest(Request.Method.GET, uri, trafiResponseListener, trafiErrorListener);
        TRAFIApplication.getInstance().addToRequestQueue(dataRequest);
    }

    @Override
    public Location getCurrentLocation() {
        return mLastLocation;
    }

    @Override
    public void showTooltip(MapMarker mapMarker) {
        tooltipText.setText(mapMarker.getTooltipTitleText());

        String uri = Uri.parse(getString(R.string.icon_request))
                .buildUpon()
                .appendQueryParameter("style", "androidv2")
                .appendQueryParameter("size", mapMarker.getMapIconSize() + "")
                .appendQueryParameter("src", mapMarker.getMapIcon())
                .appendQueryParameter("cl", "ffffff")
                .build().toString();
        String localUri = TRAFIApplication.getSqliteHelper().getImageMapping(uri);

        if (!localUri.equals("")) {
            float density = getResources().getDisplayMetrics().density;
            int px = Math.round(24 * density);
            float radius = 2 * density;
            Bitmap bm = Bitmap.createBitmap(px, px, Bitmap.Config.ARGB_8888);
            bm.eraseColor(Color.parseColor("#" + mapMarker.getColor()));
            Canvas canvas = new Canvas(bm);
            Bitmap bitmap = BitmapFactory.decodeFile(new File(localUri).getAbsolutePath());
            bitmap = Bitmap.createScaledBitmap(bitmap, bm.getWidth(), bm.getHeight(), true);
            canvas.drawBitmap(bitmap, 0, 0, null);
            tooltipIcon.setImageBitmap(Util.getRoundedCornerBitmap(bm, radius));
        }

        tooltipAnimator.expand();
    }

    @Override
    public void hideTooltip() {
        tooltipAnimator.collapse();
    }

    @Override
    public void downloadIcon(String url) {
        String fileName = url.substring(url.lastIndexOf("/") + 1, url.length());
        doDownload(url, fileName);
    }

    /**
     * Download file in another thread.
     *
     * @param urlLink  Link to download image from.
     * @param fileName Filename under which file will be created.
     */
    protected void doDownload(final String urlLink, final String fileName) {
        Thread dx = new Thread(new Runnable() {
            @Override
            public void run() {
                ContextWrapper cw = new ContextWrapper(getApplicationContext());
                File dir = cw.getDir("imageDir", Context.MODE_PRIVATE);
                if (!dir.exists()) {
                    boolean result = dir.mkdirs();

                    if (!result) {
                        return;
                    }
                }

                try {
                    URL url = new URL(urlLink);
                    URLConnection connection = url.openConnection();
                    connection.connect();
                    // download the file
                    InputStream input = new BufferedInputStream(url.openStream());
                    OutputStream output = new FileOutputStream(dir + "/" + fileName);

                    byte data[] = new byte[1024 * 6];
                    int count;

                    while ((count = input.read(data)) != -1) {
                        output.write(data, 0, count);
                    }
                    String path = dir + "/" + fileName;
                    TRAFIApplication.getSqliteHelper().addImage(urlLink, path);
                    output.flush();
                    output.close();
                    input.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        dx.setPriority(Thread.MIN_PRIORITY + 2);
        threadPoolExecutor.submit(dx);
    }
}
