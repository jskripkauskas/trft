package com.trafi.trafitest;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;

public class Util {

    /**
     * Replace color of pixels within array from one color to another one.
     *
     * @param allPixels Pixel array.
     * @param from      Color to change from.
     * @param to        Color to change to.
     * @param result    Bitmap to apply change to.
     */
    public static void replaceColor(int[] allPixels, int from, int to, Bitmap result) {
        result.getPixels(allPixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());

        for (int i = 0; i < result.getHeight() * result.getWidth(); i++) {
            if (allPixels[i] == from)
                allPixels[i] = to;
        }
        result.setPixels(allPixels, 0, result.getWidth(), 0, 0, result.getWidth(), result.getHeight());
    }

    public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, float radius) {
        Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        final int color = 0xff424242;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawRoundRect(rectF, radius, radius, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        return output;
    }
}
