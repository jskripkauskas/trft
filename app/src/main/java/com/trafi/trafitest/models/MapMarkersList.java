package com.trafi.trafitest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MapMarkersList {
    @SerializedName("MapMarkers")
    private List<MapMarker> mapMarkers;

    public List<MapMarker> getMapMarkers() {
        return mapMarkers;
    }

    public void setMapMarkers(List<MapMarker> mapMarkers) {
        this.mapMarkers = mapMarkers;
    }
}
