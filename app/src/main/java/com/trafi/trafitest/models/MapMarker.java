package com.trafi.trafitest.models;

import com.google.gson.annotations.SerializedName;

public class MapMarker {
    @SerializedName("Coordinate")
    private LatitudeLongitude coordinates;
    @SerializedName("Type")
    private int type;
    @SerializedName("MapIcon")
    private String mapIcon;
    @SerializedName("MapIconSize")
    private int mapIconSize;
    @SerializedName("ZoomLevel")
    private float zoomLevel;
    @SerializedName("Color")
    private String color;
    @SerializedName("Angle")
    private float angle;
    @SerializedName("TooltipTitleText")
    private String tooltipTitleText;
    @SerializedName("TooltipSubtitleText")
    private String tooltipSubtitleText;
    @SerializedName("VehicleId")
    private String vehicleId;
    @SerializedName("TrackStopIndexFraction")
    private float trackStopIndexFraction;

    public LatitudeLongitude getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(LatitudeLongitude coordinates) {
        this.coordinates = coordinates;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getMapIcon() {
        return mapIcon;
    }

    public void setMapIcon(String mapIcon) {
        this.mapIcon = mapIcon;
    }

    public int getMapIconSize() {
        return mapIconSize;
    }

    public void setMapIconSize(int mapIconSize) {
        this.mapIconSize = mapIconSize;
    }

    public float getZoomLevel() {
        return zoomLevel;
    }

    public void setZoomLevel(float zoomLevel) {
        this.zoomLevel = zoomLevel;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public float getAngle() {
        return angle;
    }

    public void setAngle(float angle) {
        this.angle = angle;
    }

    public String getTooltipTitleText() {
        return tooltipTitleText;
    }

    public void setTooltipTitleText(String tooltipTitleText) {
        this.tooltipTitleText = tooltipTitleText;
    }

    public String getTooltipSubtitleText() {
        return tooltipSubtitleText;
    }

    public void setTooltipSubtitleText(String tooltipSubtitleText) {
        this.tooltipSubtitleText = tooltipSubtitleText;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public float getTrackStopIndexFraction() {
        return trackStopIndexFraction;
    }

    public void setTrackStopIndexFraction(float trackStopIndexFraction) {
        this.trackStopIndexFraction = trackStopIndexFraction;
    }
}
