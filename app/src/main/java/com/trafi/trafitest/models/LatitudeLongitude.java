package com.trafi.trafitest.models;

import com.google.gson.annotations.SerializedName;

public class LatitudeLongitude {
    @SerializedName("Lat")
    private float lat;
    @SerializedName("Lng")
    private float lng;

    public float getLat() {
        return lat;
    }

    public void setLat(float lat) {
        this.lat = lat;
    }

    public float getLng() {
        return lng;
    }

    public void setLng(float lng) {
        this.lng = lng;
    }
}
