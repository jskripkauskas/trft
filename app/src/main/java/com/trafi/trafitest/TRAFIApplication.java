package com.trafi.trafitest;

import android.app.Application;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.trafi.trafitest.activities.MainActivity;
import com.trafi.trafitest.interfaces.AppComponent;
import com.trafi.trafitest.interfaces.DaggerAppComponent;

public class TRAFIApplication extends Application {
    public static final String TAG = TRAFIApplication.class.getSimpleName();
    private RequestQueue mRequestQueue;
    private static TRAFIApplication mInstance;
    private static LocalDatabaseHelper sqliteHelper;
    private static AppComponent component;

    @Override
    public void onCreate() {
        super.onCreate();

        component = DaggerAppComponent.builder().build();

        if (mInstance == null) mInstance = this;
        if (sqliteHelper == null) sqliteHelper = new LocalDatabaseHelper(this);
    }

    public static void inject(MainActivity target) {
        component.inject(target);
    }

    /**
     * Retrieves an instance of this application.
     *
     * @return Current instance of this application.
     */
    public static synchronized TRAFIApplication getInstance() {
        return mInstance;
    }

    public static LocalDatabaseHelper getSqliteHelper() {
        return sqliteHelper;
    }

    /**
     * Retrieves an existing request queue or creates a new one.
     *
     * @return Request queue.
     */
    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }

        return mRequestQueue;
    }

    /**
     * Sets request tag and add request to the dispatch queue.
     *
     * @param req Request to add to the dispatch queue.
     * @param tag Tag given to a request.
     * @param <T> Request type.
     */
    public <T> void addToRequestQueue(Request<T> req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    /**
     * Adds request to the dispatch queue with a default tag.
     *
     * @param req Request to add to the dispatch queue.
     * @param <T> Request type.
     */
    public <T> void addToRequestQueue(Request<T> req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    /**
     * Cancels requests within dispatch queue with given tag.
     *
     * @param tag Tag or requests to be canceled.
     */
    public void cancelPendingRequests(Object tag) {
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll(tag);
        }
    }
}
