package com.trafi.trafitest;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class LocalDatabaseHelper extends SQLiteOpenHelper {
    // --------------------------------VARIABLES---------------------------------------------
    protected static final int DATABASE_VERSION = 1;
    protected static final String DATABASE_NAME = "Trafi";
    protected static final String TABLE_NAME = "ImageMappings";
    protected static final String KEY_ID = "id";
    protected static final String IMAGE_ORIGINAL = "original";
    protected static final String IMAGE_LOCAL = "local";

    // -----------------------------MAIN-METHODS----------------------------------------------

    public LocalDatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE = "CREATE TABLE " + "if not exists " + TABLE_NAME
                + "(" + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + IMAGE_ORIGINAL + " TEXT," + IMAGE_LOCAL + " TEXT" + ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public synchronized void addImage(String original, String local) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(IMAGE_ORIGINAL, original);
            values.put(IMAGE_LOCAL, local);
            db.insert(TABLE_NAME, null, values);
            // db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized String getImageMapping(String original) {
        String uri = "";
        try {
            String selectQuery = "SELECT  " + IMAGE_LOCAL + " FROM " + TABLE_NAME + " WHERE "
                    + IMAGE_ORIGINAL + "=" + "'" + original + "'";
            SQLiteDatabase db = this.getWritableDatabase();
            Cursor cursor = db.rawQuery(selectQuery, null);
            try {
                if (cursor.moveToFirst()) {
                    uri = cursor.getString(0);
                }
            } finally {
                cursor.close();
                db.close();
            }
            // db.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return uri;
    }

}
