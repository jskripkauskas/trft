package com.trafi.trafitest.interfaces;

import android.location.Location;

import com.google.android.gms.maps.model.LatLngBounds;
import com.trafi.trafitest.models.MapMarker;

public interface MapInterface {

    void fetchData(String city, LatLngBounds bounds);
    Location getCurrentLocation();
    void showTooltip(MapMarker mapMarker);
    void hideTooltip();
    void downloadIcon(String url);
}
