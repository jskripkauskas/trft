package com.trafi.trafitest.interfaces;

public interface AngleInterpolator {

    float interpolate(float fraction, float a, float b);

    class SimpleInterpolator implements AngleInterpolator {
        @Override
        public float interpolate(float fraction, float a, float b) {
            return (b - a) * fraction + a;
        }
    }
}
