package com.trafi.trafitest.interfaces;

import com.trafi.trafitest.activities.MainActivity;
import dagger.Component;

@Component
public interface AppComponent {
    void inject(MainActivity activity);
}