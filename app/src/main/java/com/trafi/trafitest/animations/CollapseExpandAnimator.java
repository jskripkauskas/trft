package com.trafi.trafitest.animations;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;

public class CollapseExpandAnimator {
    protected View view;
    protected int originalHeight;

    ValueAnimator.AnimatorUpdateListener animatorUpdateListener = new ValueAnimator.AnimatorUpdateListener() {
        @Override
        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            //Update Height
            int value = (Integer) valueAnimator.getAnimatedValue();
            ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
            layoutParams.height = value;
            view.setLayoutParams(layoutParams);
        }
    };

    Animator.AnimatorListener collapseAnimatorListener = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animation) {

        }

        @Override
        public void onAnimationEnd(Animator animator) {
            view.setVisibility(View.GONE);
        }

        @Override
        public void onAnimationCancel(Animator animation) {

        }

        @Override
        public void onAnimationRepeat(Animator animation) {

        }
    };

    public CollapseExpandAnimator(View view) {
        this.view = view;
    }

    private ValueAnimator slideAnimator(int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);
        animator.addUpdateListener(animatorUpdateListener);
        return animator;
    }

    public void collapse() {
        originalHeight = view.getLayoutParams().height;
        ValueAnimator mAnimator = slideAnimator(originalHeight, 0);
        mAnimator.addListener(collapseAnimatorListener);
        mAnimator.start();
    }

    public void expand() {
        view.setVisibility(View.VISIBLE);
        ValueAnimator mAnimator = slideAnimator(0, originalHeight);
        mAnimator.start();
    }
}
